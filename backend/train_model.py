import pickle
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.metrics import r2_score, mean_squared_error
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from datetime import datetime
import pandas as pd
import re

model_file_name = "trained_model.sav"

p = pickle.load(open('history_data181019111615.pak', 'rb'))
p.extend(pickle.load(open('history_data181020171722.pak', 'rb')))
#to turn string  into integer categories, probably theres a better way to do this

conditions = []
products = []
strDates = []
for i in p:        
    strDates.append(i['date'].strftime("%Y-%m-%d"))

    if "condition_"+i['condition'] not in conditions:
        conditions.append("condition_"+i['condition'])
    if "product_"+i['product'] not in products:
        products.append("product_"+i['product'])
    capacity = 0
    capacity_match= re.search('([0-9]+)?GB', i['title'])
    if capacity_match and capacity_match.group(1):
        capacity = int(capacity_match.group(1))
    else:
        capacity = 64 

    i['capacity'] = capacity   
    i['date'] = i['date'].toordinal()
    
print(products)
print(conditions)
p_ = pd.DataFrame(p)
X = p_[['date','product','capacity','condition']]
X = pd.get_dummies(data=X)
y = p_['price']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=42)
model = LinearRegression()
model.fit(X_train, y_train)
y_pred = model.predict(X_test)

print("Coefficient: ", model.coef_)
print("Intercept: ", model.intercept_)

# X_train_date = [ item[0 ] for item in X_train]
# X_test_date =[ item[0 ] for item in X_test]
 
r_score = r2_score(y_test, y_pred)
mse = mean_squared_error(y_test, y_pred)
print("R2_score: ", r_score, ";  MSE: ", mse)


#pickle fitted model
pickle.dump(model, open(model_file_name, 'wb'))

#pickle products and conditions lists
print(list(X))
pickle.dump(list(X), open("cols_list", 'wb'))
p_[['date','product','capacity','condition', 'price']].to_csv('iPhone_data.csv')

residual = y_test - y_pred
line_x = np.linspace(0, 1000, 10)
plt.subplot(1,2,1)
plt.scatter(y_pred[:90], y_test[:90], color='blue', picker=True) #plots residual (diff. between actual vs prediction)
plt.plot(line_x,line_x, color='black')
plt.title("Actual vs Prediction plot")
plt.xlabel("Predicted Price")
plt.ylabel("Actual price")

plt.subplot(1,2,2)
plt.scatter(y_pred[:90], residual[:90], color='blue', picker=True) #plots residual (diff. between actual vs prediction)
plt.axhline(0, color='black')
plt.title("Residual vs Prediction plot")
plt.xlabel("Predicted Price")
plt.ylabel("Residual")
# ax.xaxis.set_major_locator(MaxNLocator(8))
# ax.set_xticklabels(strDates[::-int(len(strDates)/8)],rotation = 25)
plt.tight_layout()
plt.show()                                                                                                                                                                                      
            