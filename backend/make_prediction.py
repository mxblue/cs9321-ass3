import pickle
import numpy as np
from sklearn.linear_model import LinearRegression
from datetime import datetime
import pandas as pd
import random
"""
Products: 
    Apple iPhone 7, Apple iPhone 7 Plus, 
    Apple iPhone 6s, Apple iPhone 6, Apple iPhone 6 Plus, Apple iPhone 6s Plus
Conditions:
    Pre-owned 
    New (other)
"""

def make_prediction( date, condition, product, capacity):	
    model = pickle.load( open( "trained_model.sav", "rb" ) )
    cols_list =  pickle.load( open( "cols_list", "rb" ) )
    test = {}
    condition = "condition_"+condition
    product = "product_"+product
    for c in cols_list:
    	test[c] = 0
    test['date'] = datetime.strptime(date, '%y-%m-%d').toordinal()
    test['capacity'] = capacity
    test[product] = 1
    test[condition] = 1
    
    X_test = pd.DataFrame([test])
    X_test = X_test[cols_list]
    prediction = model.predict(X_test)
    return np.around(prediction, decimals=2)[0]

def display_points(date, condition, product, capacity):    
    df = pd.read_csv("iPhone_data.csv")    
    df = df.loc[df['product'] == product]
    df = df.loc[df['condition'] == condition]
    df = df.loc[df['capacity'] == int(capacity)]
    df2 = df[['date', 'price']].copy()
    test = {}
    test['history'] = []
    df2 = df2.groupby('date').mean().reset_index()
    for i,row in df2.iterrows():
        test['history'].append({ 'time': datetime.fromordinal(int(row['date'])).strftime('%y-%m-%d'), 'price': float("{0:.2f}".format(row['price']))} )
    test['estimate'] = {'time': date, 'price': make_prediction(date,condition,product,capacity)} 
    n = len(test['history'])   
    if n > 45:
        test['history'] = test['history'][n-45:n]
        
    return test



