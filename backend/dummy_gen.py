import datetime
import pickle

test_reponse = {}
test_reponse['history'] = [] 

curr_date = datetime.date(2018, 7, 19)
date_inc = datetime.timedelta(days=1)

for i in range(1, 90):
    test_reponse['history'].append( { 'time': curr_date.strftime('%y-%m-%d'), 'price': 150 - i} )
    curr_date = curr_date + date_inc

test_reponse['estimate'] = { 'time': (curr_date + 4 * date_inc).strftime('%y-%m-%d'), 'price': 30 }

print(test_reponse)

with open('testdata.pak', 'wb') as f:
    pickle.dump(test_reponse, f, -1)