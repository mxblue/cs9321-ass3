import json, pickle
from flask import Flask
from flask import request

from flask_restplus import Resource, Api, fields
from flask_cors import CORS, cross_origin
import make_prediction as mp

from functools import wraps
from flask import request,jsonify
from flask_restful import abort
from itsdangerous import (TimedJSONWebSignatureSerializer as Serializer)
SECRET_KEY = "A RANDOM KEY"

def authenticate_by_token(token):
    if token is None:
        return False
    s = Serializer(SECRET_KEY)
    username = s.loads(token.encode())
    if username == 'admin':
        return True
    return False

def login_required(f, message="You are not authorized"):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        token = request.headers.get("AUTH_TOKEN")
        print("token: ", token)
        if authenticate_by_token(token):
            return f(*args, **kwargs)
        return message, 401#jsonify(message=message), 401
    return decorated_function

# Finds the object with specified "name" value in an array of dictionaries
def find_name(dict_array, name):
    for d in dict_array:
        if "name" not in d:
            return None
        if d["name"] == name:
            return d

    return None

app = Flask(__name__)
cors = CORS(app, resources={r'/*': {"origins": '*'}})

api = Api(app,
        default="eBay Predictive Service",  # Default namespace
        title="Price estimator for eBay",  # Documentation Title
        description="Allows estimation of future prices for certain products listed on eBay.")  # Documentation Description

# Parameters currently dummy, to be replaced with correct ones based on model

product_list = {} # List of products available for prediction
param_list = {} # List of parameters, indexed by product classification

# Load values from json files
with open('products.json', 'r') as f:
    product_list = json.load(f)
with open('parameters.json', 'r') as f:
    param_list = json.load(f)

# Input model for /eps/request
col_req_payload = api.model('Collection Request', {
    "product": fields.String,
    "values": fields.String, # TODO: define this correctly
    "time": fields.DateTime()
})

# Parser for /eps/list endpoint
attr_parser = api.parser()
attr_parser.add_argument('product', type=str, help='Product to retrieve parameters for.')


@api.route('/eps/request')
class PredictiveRequest(Resource):
    @api.response(200, 'Prediction successful')
    @api.response(400, 'Invalid parameters/values')
    @api.doc(description='Predict eBay market price, given time and a product')
    @api.expect(col_req_payload)
    @login_required
    def post(self):
        payload = api.payload

        # Verify input validity

        if "product" not in payload:
            return { 'error': 'Missing product value' }, 400
        if "time" not in payload:
            return { 'error': 'Missing time value' }, 400
        if "values" not in payload:
            return { 'error': 'Missing product parameters' }, 400

        if payload['product'] not in product_list:
            return { 'error': 'Invalid product value' }, 400

        exp_params = param_list[product_list[payload['product']]['classification']]
        for exp_param in exp_params:
            exp_param_name = exp_param['name']
            values = payload['values']

            if exp_param_name not in values.keys():
                return { 'error': 'Missing parameter: ' + exp_param_name }, 400
            if find_name(exp_param['values'], values[exp_param_name]) is None:
                return { 'error': 'Invalid value for parameter ' + exp_param_name + ': ' + values[exp_param_name] }, 400

        # Get values
        model_params = payload['values']
        model_params['product'] = product_list[payload['product']]['model_ref']
        model_params['time'] = payload['time']
        
        predicted_price = mp.display_points(model_params['time'], model_params['condition'], \
            model_params['product'], model_params['capacity'])
        
        
        return predicted_price, 200

@api.route('/eps/list')
class PredictiveParameters(Resource):
    @api.response(200, 'Successful')
    @api.response(404, 'Invalid product specified')
    @api.expect(attr_parser)
    @api.doc(description='Get a list of products or their parameters.')
    def get(self):
        product = attr_parser.parse_args().get('product')

        # If product is not present, return a list of products
        if product is None:

            # Create a simplified view
            product_view = []
            for product in product_list:
                product_view.append({
                    'name': product,
                    'description': product_list[product]['description']
                })

            return product_view, 200

        # If product present, but non-existant, return error
        elif product not in product_list:
            return { 'error': 'Invalid Product' }, 400

        # If product present and existant, return list of valid parameters
        else:
            return param_list[product_list[product]['classification']], 200


# Parser for /authentication
auth_parser = api.parser()
auth_parser.add_argument('username', type=str, help='username')
auth_parser.add_argument('password', type=str, help='password')

@api.route("/auth")
class Authentication(Resource):
    @api.response(200, 'Successful')
    @api.response(404, 'Invalid username or password')
    @api.expect(auth_parser)
    @api.doc(description='Get authentication')
    def get(self):
        username = auth_parser.parse_args().get('username')
        password = auth_parser.parse_args().get('password')
        s = Serializer(SECRET_KEY, expires_in=600)

        if username != 'admin' or password != 'admin':
            return { 'error': 'Incorrect username and/or password' }, 404

        else:
            token = s.dumps(username)
            return  { 'token': token.decode() }, 200


if __name__ == '__main__':
    app.run(debug=True)
