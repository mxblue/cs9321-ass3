#!/usr/bin/python3

import requests, re, signal, sys, pickle
from dateutil import parser
from datetime import datetime, timedelta
from time import sleep
from bs4 import BeautifulSoup, SoupStrainer
import traceback

history_data = [] # Primary data store

TARGET_DELTA_DAYS = 90 # Number of days of data to fetch, max 90

EBAY_URL = 'https://www.ebay.com.au/sch/i.html'
ebay_params = { # Default parameters for fetching
    '_nkw':         'iphone+6', # Search term
    '_pgn':         1,          # Page number
    '_ipg':         200,        # 200 items per page
    'LH_Sold':      1,          # Sold listings
    'LH_Complete':  1,          # Completed listings
    'LH_Auction':   1           # Auction listings
}

# Exit handlers

# Save data to file and exit
def save_file():
    file_name = 'history_data' + datetime.now().strftime('%y%m%d%H%M%S') + '.pak'
    print('Saving to ' + file_name)
    with open(file_name, 'wb') as f:
        pickle.dump(history_data, f, -1)

# Save request HTML to file and exit
def debug_exit(raw_request):
    file_name = ebay_params['_nkw'] + '+' + datetime.now().strftime('%y%m%d%H%M%S') + '.html'
    print('Saving debug to ' + file_name)
    with open(file_name, 'wb') as f:
        f.write(raw_request.content)

    sys.exit(-1)

# Handle CTRL+C 
def exit_handler(signal, frame):
    save_file()
    sys.exit(0)
	
signal.signal(signal.SIGINT, exit_handler)

# Parsing

# Regexp for interpretting strings
r_id = re.compile('\/([0-9]+)\?')
r_date = re.compile('SOLD[ ]*(.*)')
r_price = re.compile('\$([0-9.]+)')

# Interpret listing data from ebay_parse()
def listing_parse(raw_listing):
    listing = {}

    listing['title'] = str(raw_listing['title'])
    listing['condition'] = str(raw_listing['condition'])
    if 'brand' in raw_listing:
        listing['brand'] = str(raw_listing['brand'])
    else:
        listing['brand'] = ''
    if 'product' in raw_listing:
        listing['product'] = str(raw_listing['product'])
    else:
        listing['product'] = ''
    if 'special' in raw_listing:
        listing['special'] = str(raw_listing['special'])
    else:
        listing['special'] = ''
    
    listing['id'] = int(r_id.search(raw_listing['link']).group(1))
    listing['price'] = float(r_price.search(raw_listing['price']).group(1))
    listing['date'] = parser.parse(r_date.search(raw_listing['date']).group(1))

    return(listing)
    
# SoupStrainer objects for parsing eBay search pages
s_item = SoupStrainer('div', { 'class': 's-item__info' })
s_subtitle = SoupStrainer('div', { 'class': 's-item__subtitle' })
s_item_details = SoupStrainer('span', { 'class': 's-item__price' })

# Parse eBay search pages
def ebay_parse(raw_request):
    try:
        # Create soup for the page and get listings
        bs = BeautifulSoup(raw_request.content, "html5lib")
        bs_listings = bs.find_all(s_item)

        # Return immediately if no listings exist
        if len(bs_listings) == 0:
            return 0

        # Parse individual listings
        listings = []
        for bs_listing in bs_listings:
            raw_listing = {}

            # If true, Listing has "New Listing" before the title
            if (len(bs_listing.a.h3.contents) > 2):
                # Forgive the encode/decode mess, unfortunately necessary
                raw_listing['title'] = bs_listing.a.h3.contents[2].string.encode('ascii', 'ignore').decode('ascii') # Title
            else:
                raw_listing['title'] = bs_listing.a.h3.contents[1].string.encode('ascii', 'ignore').decode('ascii') # Title

            raw_listing['link'] = bs_listing.a.get('href') # Link
            raw_listing['date'] = bs_listing.a.h3.div.string # Date sold
            
            # Mutliple subtitles may exist, in which case the first is a 'special' one
            bs_subtitles = bs_listing.find_all(s_subtitle)
            if (len(bs_subtitles) > 1):
                raw_listing['special'] = bs_subtitles[0].contents[0] # special subtitle
                del bs_subtitles[0]
            
            # The primary subtitle has the condition, and possibly the brand/product
            raw_listing['condition'] = bs_subtitles[0].contents[0].string # Condition
            if len(bs_subtitles[0].contents) > 2: # Brand exists
                raw_listing['brand'] = bs_subtitles[0].contents[2].string # Brand
            if len(bs_subtitles[0].contents) > 4: # Product exists
                raw_listing['product'] = bs_subtitles[0].contents[4].string # Product

            # Price is located inside a span deep inside the listing
            bs_item_price = bs_listing.find(s_item_details)
            raw_listing['price'] = bs_item_price.string # Price

            # Parse the data obtained and add to list
            listings.append(listing_parse(raw_listing))

        # Add obtained listings to global array
        global history_data # Use global history_data for storage
        history_data += listings

        # Return the amount of items parsed
        return len(listings)
    except:
        traceback.print_exc()
        debug_exit(raw_request)

# Fetching

time_now = datetime.now()
page_num = 1 # Start on page 1

while True:
    ebay_params['_pgn'] = page_num # Update the page number
    print(ebay_params)

    while True: # Retry parsing until success
        # Get html
        raw_request = requests.get(EBAY_URL, ebay_params)

        # Handle any failed requests
        if raw_request.status_code != 200:
            print('Request error: ', raw_request.status_code)
            debug_exit(raw_request)

        # Attempt to parse html
        if ebay_parse(raw_request) > 0:
            break

        print("Error parsing, retrying page")

    print((time_now - history_data[-1]['date']).days, "days processed,", len(history_data), "items read")
    # Test number of days of data parsed
    if (time_now - history_data[-1]['date']).days >= TARGET_DELTA_DAYS:
        break

    sleep(5) # Delay to avoid getting blocked by the server
    page_num = page_num + 1 # Increment page

# Save file and exit
save_file()