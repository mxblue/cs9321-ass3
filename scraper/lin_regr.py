import pickle
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.metrics import r2_score, mean_squared_error
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from datetime import datetime
import pandas as pd
import re

p = pickle.load(open('history_data181019111615.pak', 'rb'))

#to turn string  into integer categories, probably theres a better way to do this
conditions = []
products = []
strDates = []
for i in p:        
    strDates.append(i['date'].strftime("%Y-%m-%d"))

    if i['condition'] not in conditions:
        conditions.append(i['condition'])
    if i['product'] not in products:
        products.append(i['product'])
    capacity = 0
    capacity_match= re.search('([0-9]+)?GB', i['title'])
    if capacity_match and capacity_match.group(1):
        capacity = int(capacity_match.group(1))
    else:
        capacity = 64 

    i['capacity'] = capacity   
    i['date'] = i['date'].toordinal()
    

p_ = pd.DataFrame(p)
X = p_[['date','product','capacity','condition']]
X = pd.get_dummies(data=X)
y = p_['price']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=42)

model = LinearRegression()
model.fit(X_train, y_train)
y_pred = model.predict(X_test)

print("Coefficient: ", model.coef_)
print("Intercept: ", model.intercept_)

# X_train_date = [ item[0 ] for item in X_train]
# X_test_date =[ item[0 ] for item in X_test]
 
r_score = r2_score(y_test, y_pred)
mse = mean_squared_error(y_test, y_pred)
print("R2_score: ", r_score, ";  MSE: ", mse)
#plot all values
residual = y_test - y_pred

plt.scatter(y_pred, residual, color='blue', picker=True) #plots residual (diff. between actual vs prediction)
plt.title("Residual vs Prediction plot")
plt.xlabel("Predicted Price")
plt.ylabel("Residual")
# ax.xaxis.set_major_locator(MaxNLocator(8))
# ax.set_xticklabels(strDates[::-int(len(strDates)/8)],rotation = 25)
plt.show()                                                                                                                                                                                      
                                                                                                                    
