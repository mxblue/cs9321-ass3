import pickle
import numpy as np
from sklearn.linear_model import LinearRegression
from datetime import datetime
import pandas as pd


def make_prediction( date, condition, product, capacity):	
    model = pickle.load( open( "trained_model.sav", "rb" ) )
    cols_list =  pickle.load( open( "cols_list", "rb" ) )
    test = {}
    condition = "condition_"+condition
    product = "product_"+product
    for c in cols_list:
    	test[c] = 0
    test['date'] = date.toordinal()
    test['capacity'] = capacity
    test[product] = 1
    test[condition] = 1
    
    X_test = pd.DataFrame([test])
    X_test = X_test[cols_list]
    prediction = model.predict(X_test)
    return np.around(prediction, decimals=2)

"""
Products: 
	Apple iPhone 7, Apple iPhone 7 Plus, 
	Apple iPhone 6s, Apple iPhone 6, Apple iPhone 6 Plus, Apple iPhone 6s Plus
Conditions:
	Pre-owned 
	New (other)
"""

print(" predicted: ", make_prediction(datetime.strptime('2018-10-21', '%Y-%m-%d'), 'New (other)', 'Apple iPhone 6 Plus', 128 ))




