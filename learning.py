import numpy as np
from sklearn.linear_model import LinearRegression

requested_time = np.array([7])#set this to the time requested by the user
requested_time= requested_time.reshape(-1, 1) #data is single feature

X = np.array([1,2,3,4,5,6]) #training data, set this to previous times
X= X.reshape(-1, 1) #data is single feature
y = np.array([10, 10, 9, 9, 8, 8])#target values, set this to the corresponding prices
reg = LinearRegression().fit(X, y)
print("score: ", reg.score(X, y))
print( "reg.coef_: ", reg.coef_)
print( "reg.intercept_: ", reg.intercept_ )
predicted_price =  reg.predict(requested_time)

print(predicted_price)
