import React from 'react';
import ReactDOM from 'react-dom';
import './css/index.css';
import Main from './views/Main';
import registerServiceWorker from './registerServiceWorker';
import 'bulma/css/bulma.css';

import { Provider } from 'mobx-react';

import itemStore from './stores/itemStore';
const stores = {
    itemStore
  };

ReactDOM.render(<Provider {...stores}><Main /></Provider>, document.getElementById('root'));
registerServiceWorker();
