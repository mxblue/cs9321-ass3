import React, { Component } from "react";
import { Route, HashRouter } from "react-router-dom";

import Home from "./Home";
import ItemSelect from "./ItemSelect";
import PricePrediction from "./PricePrediction";

class Main extends Component {

  render() {

    return (
      <HashRouter>
        <div className="content">
          <Route exact path="/" component={Home}/>
          <Route path="/ItemSelect" component={ItemSelect}/>
          <Route path="/PricePrediction" component={PricePrediction}/>
        </div>
      </HashRouter>
    );
  }
}

export default Main;