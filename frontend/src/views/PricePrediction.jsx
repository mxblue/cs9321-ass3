import React, { Component } from 'react';
import '../css/App.css';
import Select from 'react-select';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import { inject, observer } from 'mobx-react';
import 'react-datepicker/dist/react-datepicker.css';

@inject('itemStore')
@observer
class PricePrediction extends Component {
  componentWillUnmount() {
    this.props.itemStore.reset();
  }

  componentDidMount() {
    this.props.itemStore.getItemParameters();
  }

  handleParameterChange = (e, name) => this.props.itemStore.setParameters(e, name);

  handleSendParameters = (e, name) => this.props.itemStore.sendParameters();

  handleTargetDateChange = date => this.props.itemStore.setTargetDate(date);

  getSelectedValue(name) {
    if (name === 'capacity') {
        return this.props.itemStore.capacity;
    } else if (name === 'condition') {
        return this.props.itemStore.description;
    }
  }

  render() {

    const { values } = this.props.itemStore;
    var LineChart = require("react-chartjs").Line;

    var temp = JSON.stringify(values.chartData);
    var temp = JSON.stringify(values.chartOptions);



    var predictionDisplay;
    if (values.estimateValue === '') {
        predictionDisplay = <div/>
    } else {
        predictionDisplay =
        <div>
            <div style={{ 'display':'inline', 'color':'white'}}>Estimated price for </div>
            <div style={{ 'display':'inline', 'color':'#ff3860'}}>{values.estimateDate} </div>
            <div style={{ 'display':'inline', 'color':'white'}}>is </div>
            <div style={{ 'display':'inline', 'color':'#ff3860'}}>${values.estimateValue}</div>
            <div className="button is-warning" style={{'margin':'15px', 'float': 'right'}} onClick={()=>(window.scrollBy(0,-1000))}> <i className="fa fa-chevron-up"></i> </div>

        </div>
    }

    var chartDisplay;
    if (values.estimateValue === '') {
        chartDisplay = <div/>
    } else {
        chartDisplay = <LineChart data={values.chartData} options={values.chartOptions} width="1350px" height="650px"/>
    }

    return (
      <div className="App">
          <section className="hero is-info is-fullheight">
            <div className="hero-head">
                <nav className="navbar">
                    <div className="container">
                        <div className="navbar-brand">
                            <a className="button is-warning is-large is-outlined"
                                style={{'margin':'20px'}}
                                href="/#/ItemSelect" >
                                <i class="fas fa-angle-left" style={{'margin-right':'8px'}}></i>
                                <span>Back</span>
                            </a>
                        </div>
                    </div>
                </nav>
            </div>
            <div className="container">
                <div className="landingtitle" style={{'font-family':'avenir,sans-serif', 'line-height':'1em','font-size':'150px', 'font-weight':'700'}}>
                    <div style={{ 'color':'#209cee'}}>{values.itemName}</div>
                </div>
                <h1 className="subtitle" style={{'margin':'0px', 'color':'#ffdd57', 'padding-top':'0', 'padding-bottom':'2rem'}}>
                    Select Parameters
                </h1>
                <div style={{'margin-left':'25rem', 'margin-right':'25rem'}}>
                    {values.itemParameters.map((element, index) => (
                        <div className='box' style={{'padding':'0px', 'background':'transparent'}}>
                            <form>
                                <div className="field">
                                    <div className="control">
                                        <label>
                                        <h3 style={{'color':'#ff3860'}}>{element.description}</h3>
                                        <Select
                                            options={element.values.map(x => ({ label: x.description, value: x.name }))}
                                            value={this.getSelectedValue(element.name)}
                                            onChange={(e) => this.handleParameterChange(e, element.name)}
                                        />
                                        </label>
                                    </div>
                                </div>
                            </form>
                        </div>
                    ))}
                    <form style={{'margin-bottom':'25px'}}>
                        <div className="field">
                            <div className="control">
                                <label>
                                <h3 style={{'color':'#ff3860'}}>Target Date</h3>
                                <DatePicker
                                    selected={moment(values.targetDate)}
                                    onChange={this.handleTargetDateChange}
                                    dateFormat="YY-MM-DD"
                                />
                                </label>
                            </div>
                        </div>
                    </form>
                    <a className="button is-medium is-rounded is-primary"
                        style={{'padding-left':'50px', 'padding-right':'50px', 'font-size':'1.5rem'}}
                        onClick={this.handleSendParameters}>
                        <span><b>Predict Prices</b></span>
                    </a>
                </div>
            </div>
            <div className="landingtitle" style={{'font-family':'avenir,sans-serif', 'line-height':'1em','font-size':'70px', 'font-weight':'700', 'margin-top':'4rem','margin-left':'25rem', 'margin-right':'25rem'}}>
                {predictionDisplay}
            </div>
            <div>
                {chartDisplay}
            </div>
        </section>
    </div>
    );
  }
}

export default PricePrediction;
