import React, { Component } from 'react';
import '../css/App.css';
import { inject, observer } from 'mobx-react';

@inject('itemStore')
@observer
class Home extends Component {

    handleUsernameChange = e => this.props.itemStore.setUsername(e.target.value);
    handlePasswordChange = e => this.props.itemStore.setPassword(e.target.value);
    handleLogin = (e) => {
        this.props.itemStore.getAuth();
      };
    

  render() {

    const { values } = this.props.itemStore;


    return (
        <div className="App">
          <section className="hero is-info is-fullheight">
            <div className="hero-head">

            </div>

            <div className="hero-body">
                <div className="container has-text-centered">
                    <div className="column is-8 is-offset-2">
                        <div className="landingtitle" style={{'font-family':'avenir,sans-serif', 'line-height':'0.75em','font-size':'200px', 'font-weight':'700'}}>
                            <div style={{'display':'inline', 'color':'#ff3860'}}>e</div>
                            <div style={{'display':'inline', 'color':'#3273dc'}}>b</div>
                            <div style={{'display':'inline', 'color':'#ffdd57'}}>a</div>
                            <div style={{'display':'inline', 'color':'#00d1b2'}}>y</div>
                        </div>
                        <h1 className="subtitle">
                            Price Prediction RESTful API Service
                        </h1>
                        <div>
                            <div className="column is-6 is-offset-3">
                                <div className="box register">
                                <h2>Sign into your account</h2>
                                <form>
                                    <div className="field">
                                        <div className="control">
                                        <input 
                                        className="input is-large" 
                                            type="username" 
                                            placeholder="User Name" 
                                            value={values.username}
                                            onChange={this.handleUsernameChange}
                                            autoFocus=""/>
                                        </div>
                                    </div>

                                    <div className="field">
                                        <div className="control">
                                        <input className="input is-large" 
                                        type="password" 
                                        placeholder="Password"
                                        value={values.password}
                                        onChange={this.handlePasswordChange}/>
                                        </div>
                                    </div>  
                                    {//loginError
                                    }
                                    
                            
                                    <button 
                                        className="button is-primary is-block is-info is-large is-fullwidth"
                                        href="/#/ItemSelect"
                                        onClick={this.handleLogin} >
                                        Login
                                    </button>

                                </form>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
          </section>
        </div>
    );
  }
}

export default Home;
