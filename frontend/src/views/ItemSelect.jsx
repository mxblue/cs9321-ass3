import React, { Component } from 'react';
import '../css/App.css';
import { inject, observer } from 'mobx-react';
import moment from 'moment';

@inject('itemStore')
@observer
class ItemSelect extends Component {  
  componentWillUnmount() {
  }

  componentDidMount() {
    this.props.itemStore.getItemList();
  }
  
  // Debugging function. Print out the state. 
  handleShowInfo = (e) => {
    e.preventDefault();
  };  
    
  handleSelectItem = (e,element) => {
    console.log(element.name);
    console.log(element.description);
    this.props.itemStore.setSelectedItem(element);
  };  

  render() {

    const { values } = this.props.itemStore;

    return (
      <div className="App">
          <section className="hero is-info is-fullheight">
          <div className="hero-head">
                <nav className="navbar">
                    <div className="container">
                        <div className="navbar-brand">
                            <a className="button is-warning is-large is-outlined"
                                style={{'margin':'20px'}}
                                href="/#/" >      
                                <i class="fas fa-angle-left" style={{'margin-right':'8px'}}></i>                
                                <span>Log Out</span>
                            </a>  
                        </div>
                    </div>
                </nav>
            </div>
            <div className="hero-body">
                <div className="container">
                    <div className="columns">
                      <div className="column"> 
                        <div className="landingtitle" style={{'margin-top':'100px','font-family':'avenir,sans-serif', 'line-height':'1em','font-size':'150px', 'font-weight':'700'}}>
                            <div style={{'display':'inline', 'color':'#ffdd57'}}>Select a product:</div>
                        </div>
                        <h1 className="subtitle">
                          Click on an item to recieve predictions
                        </h1> 
                      </div>
                      <div className="column"> 
                        {
                          // Conditionally Render a button for each item in the
                        }
                        {values.itemList.map((element, index) => (
                          <div style={{'margin':'40px'}}>
                            <a className="button is-large is-fullwidth is-rounded is-danger" 
                              style={{'padding-left':'60px', 'padding-right':'60px', 'font-size':'1.75rem'}}
                              onClick={(e) => this.handleSelectItem(e,element)} 
                              href={'/#/PricePrediction/'+element.name}>      
                              <span><b>> {element.description}</b></span>
                            </a>
                          </div>
                        ))}
                      </div>
                    </div>
                </div>
            </div>
          </section>

      </div>
    );
  }
}

export default ItemSelect;
