import { observable, action } from 'mobx';
import moment from 'moment';

class ItemStore {
	@observable inProgress = false;
	@observable errors = undefined;

	// Values available for access by components
	@observable values = {
		itemList: [],
		selectedItem: '',
		itemName:'',
		itemParameters: [],
		condition: '',
		capacity: '',
		history: [],
		targetDate: moment(),
		username:'',
		password:'',
		estimateDate: '',
		estimateValue: '',
		chartOptions: {

			///Boolean - Whether grid lines are shown across the chart
			scaleShowGridLines : false,

			//String - Colour of the grid lines
			scaleGridLineColor : "rgba(255,255,255,0.8)",

			//Number - Width of the grid lines
			scaleGridLineWidth : 2,

			//Boolean - Whether to show horizontal lines (except X axis)
			scaleShowHorizontalLines: true,

			//Boolean - Whether to show vertical lines (except Y axis)
			scaleShowVerticalLines: true,

			//Boolean - Whether the line is curved between points
			bezierCurve : true,

			//Number - Tension of the bezier curve between points
			bezierCurveTension : 0.4,

			//Boolean - Whether to show a dot for each point
			pointDot : true,

			//Number - Radius of each point dot in pixels
			pointDotRadius : 5,

			//Number - Pixel width of point dot stroke
			pointDotStrokeWidth : 1,

			scaleFontSize: 16,

			showTooltips: false,

			//Number - amount extra to add to the radius to cater for hit detection outside the drawn point
			pointHitDetectionRadius : 3,

			//Boolean - Whether to show a stroke for datasets
			datasetStroke : false,

			//Number - Pixel width of dataset stroke
			datasetStrokeWidth : 2,

			//Boolean - Whether to fill the dataset with a colour
			datasetFill : false,

			//Boolean - Whether to horizontally center the label and point dot inside the grid
			offsetGridLines : false
		},
		chartData:{
			labels: [],
			datasets: [
				{
					label: "Price",
					fillColor: "rgba(220,220,220,0.2)",
					strokeColor: "transparent",
					pointColor: "#ffdd57",
					pointStrokeColor: "#ffdd57",
					pointHighlightFill: "#ffdd57",
					pointHighlightStroke: "#ffdd57",
					data: []
				}
			]
		},

	};

	// Clear state upon changing page
	@action reset() {
		this.values.itemList = [];
		//this.values.selectedItem = '';
		this.values.itemParameters = [];
		this.values.condition = '';
		this.values.history = [];
		this.values.targetDate = moment();
		this.values.estimateDate = '';
		this.values.estimateValue = '';
		this.values.chartData={
			labels: [],
			datasets: [
				{
					label: "Price",
					fillColor: "rgba(220,220,220,0.2)",
					strokeColor: "transparent",
					pointColor: "#ffdd57",
					pointStrokeColor: "#ffdd57",
					pointHighlightFill: "#ffdd57",
					pointHighlightStroke: "#ffdd57",
					data: []
				}
			]
		}
	}

	// Helper function for debugging. Print contents
	@action printState() {
		console.log('Items List: ' + JSON.stringify(this.values.itemList));
		console.log('Selected Item: ' + this.values.selectedItem);
		console.log('Item Name: ' + this.values.itemName);
		console.log('Item Parameters: ' + this.values.itemParameters);
	}

	@action setSelectedItem(item) {
		this.values.selectedItem = item;
	}

	@action setTargetDate(date) {
		this.values.targetDate = date;
	}

	@action setUsername(input) {
		this.values.username = input;
	}

	@action setPassword(input) {
		this.values.password = input;
	}

	@action getAuth(){
		var username = this.values.username;
		var password = this.values.password;

		console.log("user:", username, "password: ", password);
		return fetch('http://127.0.0.1:5000/auth?username='+username +'&password=' + password,{
			method: 'get',
			headers: {
				"Accept" : "application/json"},
		}).then(res => {
			console.log('GET request completed.', res);

			if (res.status == 404) {
				alert('Sign In failed. Please check your user name and password.');
			} else {
				let response = res.json().then( (json) => {
					console.log("Res; ",json)
					let token = json.token;
					//this.values.token = token
					sessionStorage.setItem('token', token);
					alert('Sign In Successful.');
					window.location.replace('http://localhost:3000/#/ItemSelect')
				})
			}
		});
	}

	// GET /eps/list
	@action getItemList() {
		console.log('Run /eps/list');

		this.inProgress = true;
		this.errors = undefined;

		return fetch('http://127.0.0.1:5000/eps/list', {
			method: 'get',
			headers: {
				"Accept" : "application/json"},
		}).then(res => {
			console.log('GET request completed.');
			return res.json();
		}).then(res => {
			console.log(res);
			this.values.itemList=res;
		}).finally(
			action(() => {this.inProgress = false;
			})
		);

	}

	// GET /eps/list?product=
	@action getItemParameters() {
		console.log('Run /eps/list?product=');

		this.inProgress = true;
		this.errors = undefined;

		// We get the item to get parameters for from the current
		// URL. The URL is set by a (dynamically rendered) button in ItemSelect.jsx
		var currentUrl = window.location.href;
		var product = currentUrl.replace('http://localhost:3000/#/PricePrediction/','');
		this.values.itemName = product;

		return fetch('http://127.0.0.1:5000/eps/list?product='+ product, {
			method: 'get',
			headers: {
				"Accept" : "application/json"},
		}).then(res => {
			console.log('GET request completed.');
			return res.json();
		}).then(res => {
			console.log(res);
			this.values.itemParameters=res;
		}).finally(
			action(() => {this.inProgress = false;
			})
		);

	}

	@action setParameters(e, name) {
		if (name === 'condition') {
			this.values.condition = e.value;
		} else if (name === 'capacity') {
			this.values.capacity = e.value;
		}
	}

	// POST/eps/request
	@action sendParameters() {
		console.log(this.values.itemName);
		//console.log(moment().format('YYYY-MM-DD').toString() + 'T' + moment().format('HH:mm:ss.SSS').toString() + 'Z');
		var date = this.values.targetDate.format('YY-MM-DD').toString();
		console.log(date);
		console.log(this.values.condition);
		console.log(this.values.capacity);
		console.log(this.values.token);

		// If fields are not filled out, return
		if (this.values.itemName === '' || this.values.condition === '' || this.values.capacity === '') {
			return;
		}
		console.log("making request, token is:",  this.values.token)
		return fetch('http://127.0.0.1:5000/eps/request', {
			method: 'post',
			headers: {
				"Accept" : "application/json",
				"Content-Type" : "application/json",
				"AUTH_TOKEN" :  sessionStorage.getItem("token")//this.values.token
			},
			body: JSON.stringify({
				'product': this.values.itemName,
				"values": {
					"condition": this.values.condition.toString(),
					"capacity": this.values.capacity.toString()
				},
				'time': date
				// Time needs to be changed/selected manually
			})
		}).then(res => {
			console.log('POST request completed.');
			return res.json();
		}).then(res => {
			console.log(res);
			this.values.history = res.history;
			this.values.estimateDate = res.estimate.time;
			this.values.estimateValue = res.estimate.price;

			// Extract the time values from the response
			// and place them in our chart data object
			console.log(res.history.length);
			var timeArray = [];
			for (var i = 0; i < res.history.length; i++) {
				var timeValue = res.history[i].time;
				timeArray.push(timeValue);
				console.log(i);
			}

			// Add the estimated time value onto the end
			var estValue = res.estimate.time;
			timeArray.push(estValue);
			this.values.chartData.labels = timeArray;

			// Extract the price values from the response
			// and place them in out chart data object
			var valueArray = [];
			for (var i = 0; i < res.history.length; i++) {
				var priceValue = res.history[i].price;
				valueArray.push(priceValue);
			}

			// Add the estimated price to our array
			var estPriceValue = res.estimate.price;
			valueArray.push(estPriceValue);
			this.values.chartData.datasets[0].data = valueArray;

			window.scrollBy(0,800);

		}).finally(
			action(() => {this.inProgress = false;
			})
		);

	}
}

export default new ItemStore();
